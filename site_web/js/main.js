

/* ------ { Animations } ------ */
$(document).ready(function () {
    $('.animateUp').css('opacity', 0);
    $('.animateLeft').css('opacity', 0);
    $('.animateRight').css('opacity', 0);
    $('.splitLeft').css('opacity', 0);
    $('.splitRight').css('opacity', 0);
    $('.back-to-top').css('opacity', 0);
    $('.animateUp').each(function () {
        var self = $(this);
        $(this).waypoint({
            handler: function () {
                self.addClass('fadeInUp');
                self.css('opacity', 1);
            },
            offset: "95%"
        })
    });
    $('.animateLeft').each(function () {
        var self = $(this);

        $(this).waypoint({
            handler: function () {
                self.addClass('fadeInLeft');
            },
            offset: '95%'
        })
    });
    $('.animateRight').each(function () {
        var self = $(this);

        $(this).waypoint({
            handler: function () {
                self.addClass('fadeInRight');
            },
            offset: '95%'
        })
    });
    $('.splitLeft').each(function () {
        var self = $(this);

        $(this).waypoint({
            handler: function () {
                self.addClass('fadeInDown');
            },
            offset: '15%'
        })
    });
    $('.splitRight').each(function () {
        var self = $(this);

        $(this).waypoint({
            handler: function () {
                self.addClass('fadeInUp');
            },
            offset: '15%'
        })
    });
});


window.onscroll = function() {
    var scroll = $(window).scrollTop();
    if(scroll == 0){
        $('.animateUp').css('opacity', 0);
        $('.animateUp').removeClass('fadeInUp');
        $('.animateLeft').css('opacity', 0);
        $('.animateLeft').removeClass('fadeInLeft');
        $('.animateRight').css('opacity', 0);
        $('.animateRight').removeClass('fadeInRight');
    }
};


/* ----- { SLIDER } ------ */

$(document).ready(function (){
    if(document.getElementById("slideshow")){
        var slide_data = slides;
        var result = "";

        for(var i=0; i< slide_data.length; i++){
            var item = slide_data[i];
            var img = item.src;
            if(img != undefined && img != ""){
                result += '<div class="carousel-item';
                if(i==0){result+=' active';}
                result += '">';
                result += '<div class="content"';
                result += ' data-src="'+img+'">';
                result += '<div class="d-flex align-items-center justify-content-center h-100 w-100 flex-column text-center"> <div class="text-center d-flex flex-column justify-content-center align-items-center">';
                if(item.titre && item.titre!=""){
                    result += '<h1 class="text-white">' + item.titre + '</h1>';
                }
                if(item.description && item.description!=""){
                    result += '<p class="text-white bold">'+item.description+'</p>';
                }
                result+='</div></div></div></div>';
            }
        }

        var container = document.getElementById("slideshow");
        container.innerHTML = result;


        /*var slide_img = $("#slideshow .slide:first");
        var slide_height = slide_img.height();
        var slide_width = slide_img.width();
        var slide_padding = parseInt(slide_img.css("paddingBottom"));


        $(".hero").css("height",slide_padding + slide_height);*/

        $('.carousel-item .content').each(function () {
            var self = $(this);
            self.css("background-image","url(" + self.data("src") + ")");
        });
    }
    
});

/*window.onresize = function(event) {
    var slide_padding = parseInt($("#slideshow .slide:first").css("paddingBottom"));
    var slide_height = $("#slideshow .slide:first").height();
    $(".hero").css("height",slide_padding + slide_height);
};*/