var slides = [
    
    {   // premiere slide
        src:"img/backgrounds/hero.jpg", 
        titre:"Maison Aloïs Alzheimer", 
        description:"Respect. Chaleur humaine. Empathie."
    },
    
    {   // troisième slide
        src:"img/backgrounds/hero2.jpg", 
        titre:"Une culture de bénévolat bien vivante", 
        description:"Joignez-vous à notre équipe et faites une différence dans votre communauté."
    },
    
    {   // quatrième slide slide
        src:"img/backgrounds/bg-soiree.jpg", 
        titre:"Soirée en Bonne Compagnie", 
        description:"Des moments de qualité entre aidants et aidés <br> au sein d’un milieu de vie adapté, deux jeudis par mois."
    }
];